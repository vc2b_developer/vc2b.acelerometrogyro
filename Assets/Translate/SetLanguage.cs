﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLanguage : MonoBehaviour {

    void Awake()
    {
        //first we'll set "en" (english) as the default language
        LanguageDictonary.SetLanguage(Application.systemLanguage);

        //if the system language isn't included in here, then the game will show the texts only in the default language

        Debug.Log("Language set: " + Application.systemLanguage);
    }

    public void ChangeEnglish()
    {
        LanguageDictonary.SetLanguage(SystemLanguage.English);
        Debug.Log("Language set: " + Application.systemLanguage);
    }
    public void ChangePortuguease()
    {
        LanguageDictonary.SetLanguage(SystemLanguage.Portuguese);
        Debug.Log("Language set: " + Application.systemLanguage);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class detectAcelerometer : MonoBehaviour {

    public Text txtAcelX, txtAcelY, txtAcelZ;

    public Slider sldAcelX, sldAcelY, sldAcelZ;

    public Text txtGyroX, txtGyroY, txtGyroZ;

    public Slider sldGyroX, sldGyroY, sldGyroZ;

    // Update is called once per frame
    void Start()
    {
        Input.gyro.enabled = true;
    }

    void Update () {
        txtAcelX.text = "" +Input.acceleration.x;
        txtAcelY.text = "" + Input.acceleration.y;
        txtAcelZ.text = "" + Input.acceleration.z;

        sldAcelX.value = Input.acceleration.x;
        sldAcelY.value = Input.acceleration.y;
        sldAcelZ.value = Input.acceleration.z;


        txtGyroX.text = "" + Input.gyro.rotationRate.x;
        txtGyroY.text = "" + Input.gyro.rotationRate.y;
        txtGyroZ.text = "" + Input.gyro.rotationRate.z;

        


    }
}
